package com.epam.edu.online.tasks;

import com.epam.edu.online.model.NumberArray;
import com.epam.edu.online.model.Plateau;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class PlateauTest {

    private static Plateau plateau;

    @BeforeAll
    static void init() {
        plateau = new Plateau();
    }

    @Test
    void testGetLongestPlateau() {
        List<Integer> integerList = new ArrayList<>(Arrays.asList(1, 2, 2, 3, 4, 4, 2, 2, 2, 2, 3, 3, 3, 4, 4, 5, 5, 5));
        assertEquals(2, plateau.getLongestPlateau(integerList).getValue());
    }
    @Test
    void testGetLongestPlateauWithEmptyInputData() {
        List<Integer> integerList = new ArrayList<>();
        assertNull(plateau.getLongestPlateau(integerList));
    }
    @Test
    void testGetLongestPlateauWithoutInputData() {
        assertThrows(NullPointerException.class, () -> plateau.getLongestPlateau(null));
    }

    @Test
    void testGetRepeatedNumbersWithoutInputData() {
        assertThrows(NullPointerException.class, () -> plateau.getRepeatedNumbers(null));
    }
    @Test
    void testGetRepeatedNumbers() {
        List<Integer> integerList = Arrays.asList(1, 2, 2, 3, 4, 4, 2, 2, 2, 2, 3, 3, 3, 4, 4, 5, 5, 5);
        List<NumberArray> repeatedNumbers = plateau.getRepeatedNumbers(integerList);
        assertEquals(repeatedNumbers.get(repeatedNumbers.size() - 1).getLastIndex(), integerList.size());
    }
}


