package com.epam.edu.online.tasks;

import com.epam.edu.online.model.Level;
import com.epam.edu.online.model.Minesweeper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class MinesweeperTest {

    private static Minesweeper minesweeper;

    @BeforeAll
    static void init() {
        minesweeper = new Minesweeper();
    }

    @Test
    void testBuildArea() {
        assertEquals(25, minesweeper.buildArea(25, Level.EASY).length);
    }
    @Test
    void testBuildAreaNegativeSize() {
        assertThrows(NegativeArraySizeException.class, () -> minesweeper.buildArea(-25, Level.EASY));
    }
}
