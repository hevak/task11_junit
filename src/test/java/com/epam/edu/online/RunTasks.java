package com.epam.edu.online;

import org.junit.platform.runner.JUnitPlatform;
import org.junit.platform.suite.api.SelectPackages;
import org.junit.platform.suite.api.SuiteDisplayName;
import org.junit.runner.RunWith;

@RunWith(JUnitPlatform.class)
@SuiteDisplayName("Suite tasks runner")
@SelectPackages("com.epam.edu.online.tasks")
public class RunTasks {
}
