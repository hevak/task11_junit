package com.epam.edu.online.controller;

import com.epam.edu.online.model.Level;
import com.epam.edu.online.model.NumberArray;

import java.util.List;

public interface Controller {

    NumberArray longestPlateau(List<Integer> integerList);
    void buildAndShowMinesweeper(int size, Level level);
    void showNumberOfBomb();

}
