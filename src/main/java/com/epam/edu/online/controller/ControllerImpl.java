package com.epam.edu.online.controller;

import com.epam.edu.online.model.Level;
import com.epam.edu.online.model.Minesweeper;
import com.epam.edu.online.model.NumberArray;
import com.epam.edu.online.model.Plateau;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;

public class ControllerImpl implements Controller {
    private final static Logger log = LogManager.getLogger(ControllerImpl.class);

    private Minesweeper minesweeper;

    public ControllerImpl() {
        minesweeper = new Minesweeper();
    }

    @Override
    public NumberArray longestPlateau(List<Integer> integerList) {
        return new Plateau().getLongestPlateau(integerList);
    }

    @Override
    public void buildAndShowMinesweeper(int size, Level level) {
        minesweeper.showArea(size, level);
    }

    @Override
    public void showNumberOfBomb() {
        minesweeper.showNumberOfBomb();
    }
}
