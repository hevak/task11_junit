package com.epam.edu.online.view;

import com.epam.edu.online.controller.Controller;
import com.epam.edu.online.controller.ControllerImpl;
import com.epam.edu.online.model.Level;
import com.epam.edu.online.model.Printable;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;

public class MyView {
    private final static Logger log = LogManager.getLogger(MyView.class);

    public static Scanner scanner = new Scanner(System.in);
    private Map<String, String> menu = new LinkedHashMap<>();
    private Map<String, Printable> methodMenu = new LinkedHashMap<>();
    private Controller controller = new ControllerImpl();
    private ResourceBundle bundle;

    public MyView() {
        bundle = ResourceBundle.getBundle("menu");
        setMenu();
        setMenuMethods();
    }

    private void setMenuMethods() {
        methodMenu.put("1", () ->
                log.info(controller.longestPlateau(Arrays.asList(1, 2, 2, 3, 4, 4, 2, 2, 2, 2, 3, 3, 3, 4, 4, 5, 5, 5))));
        methodMenu.put("2", () -> {
            System.out.print("Enter size: ");
            controller.buildAndShowMinesweeper(scanner.nextInt(), Level.MIDDLE);
        });
        methodMenu.put("3", () -> controller.showNumberOfBomb());
        methodMenu.put("q", () -> log.info("good by"));
    }

    private void setMenu(){
        menu = new LinkedHashMap<>();
        menu.put("1", bundle.getString("1"));
        menu.put("2", bundle.getString("2"));
        menu.put("3", bundle.getString("3"));
    }

    private void printMenu() {
        System.out.println();
        menu.forEach((key, value) -> System.out.println(key + "\t - " + value));
        log.info("Enter number: ");
    }

    public void run(){
        String input = "";
        do {
            try {
                printMenu();
                input = MyView.scanner.next().toLowerCase();
                methodMenu.get(input).print();
            } catch (NullPointerException e) {
                log.error("wrong command");
                printMenu();
            } catch (Exception e) {
                e.printStackTrace();
                log.error(e.getMessage());
            }
        } while (!input.equalsIgnoreCase("Q"));
    }


}
