package com.epam.edu.online.model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class Plateau {
    private final static Logger log = LogManager.getLogger(Plateau.class);

    public List<NumberArray> getRepeatedNumbers(List<Integer> integerList) {
        List<NumberArray> countRepeatIntegers = new ArrayList<>();
        NumberArray previous = new NumberArray();
        for (int i = 0; i < integerList.size(); i++) {
            if (!integerList.get(i).equals(previous.getValue())) {
                countRepeatIntegers.add(new NumberArray(integerList.get(i), i, i+1));
            } else {
                countRepeatIntegers.get(countRepeatIntegers.size() - 1).setLastIndex(i+1);
            }
            previous = countRepeatIntegers.get(countRepeatIntegers.size() - 1);
        }
        return countRepeatIntegers;
    }

    public NumberArray getLongestPlateau(List<Integer> integerList) {
        List<NumberArray> repeatedNumbers = getRepeatedNumbers(integerList);
        repeatedNumbers.forEach(log::info);
        return repeatedNumbers
                .stream()
                .max(Comparator.comparingInt(o -> (o.getLastIndex() - o.getFirstIndex())))
                .orElse(null);

    }

}
