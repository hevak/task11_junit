package com.epam.edu.online.model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Random;

public class Minesweeper {
    private final static Logger log = LogManager.getLogger(Minesweeper.class);

    private boolean[][] area;

    public void showArea(int size, Level probability) {
        area = buildArea(size, probability);
        for (boolean[] row : area) {
            System.out.print("|");
            for (boolean cell : row) {
                if (cell) {
                    System.out.print("*|");
                } else {
                    System.out.print(" |");
                }
            }
            System.out.println();
        }
    }

    public boolean[][] buildArea(int size, Level probability) {
        area = new boolean[size][size];
        for (int row = 0; row < area.length; row++) {
            for (int cell = 0; cell < area[row].length; cell++) {
                area[row][cell] = new Random().nextInt(probability.getLevel()) == 0;
            }
        }
        return area;
    }

    public void showNumberOfBomb() {
        if (area == null) {
            log.error("first build minesweeper");
            return;
        }
        int rows = area.length;
        for (int row = 0; row < rows; row++) {
            System.out.print("|");
            int cells = area[row].length;
            for (int cell = 0; cell < cells; cell++) {
                if (area[row][cell]) {
                    System.out.print("*|");
                } else {
                    int number = countNumber(row, cell);
                    System.out.print(number == 0 ? " |" : number + "|");
                }
            }
            System.out.println();
        }
    }

    /**
     * @param row row
     * @param cell cell
     * @return count neighboring bombs (above, below, left, right, or diagonal)
     */
    private int countNumber(int row, int cell) {
        int counter = 0;
        if ((row != 0) && (cell != 0) && (area[row - 1][cell - 1])) {
            counter++;
        }// left up
        if ((row != 0) && (area[row - 1][cell])) {
            counter++;
        }// up
        if ((row != 0) && (cell != area[cell].length - 1) && (area[row - 1][cell + 1])) {
            counter++;
        }// right up
        if ((cell != 0) && (area[row][cell - 1])) {
            counter++;
        }// left
        if ((cell != area[cell].length - 1) && (area[row][cell + 1])) {
            counter++;
        }// right
        if ((row != area[row].length - 1) && (cell != 0) && (area[row + 1][cell - 1])) {
            counter++;
        }// left down
        if ((row != area[row].length - 1) && (area[row + 1][cell])) {
            counter++;
        }// down
        if ((row != area[row].length - 1) && (cell != area[cell].length - 1) && (area[row + 1][cell + 1])){
            counter++;
        }// right down
        return counter;
    }
}
