package com.epam.edu.online.model;

public enum Level {
    EASY(8), MIDDLE(5), HARD(3);

    private Level(int level) {
        this.level = level;
    }

    private int level;

    public int getLevel() {
        return level;
    }
}
