package com.epam.edu.online.model;

public class NumberArray {
    private int value;
    private int firstIndex;
    private int lastIndex;

    public NumberArray() {
    }

    public NumberArray(int value, int firstIndex, int lastIndex) {
        this.value = value;
        this.firstIndex = firstIndex;
        this.lastIndex = lastIndex;
    }

    public int getValue() {
        return value;
    }

    public int getFirstIndex() {
        return firstIndex;
    }

    public int getLastIndex() {
        return lastIndex;
    }

    public void setLastIndex(int lastIndex) {
        this.lastIndex = lastIndex;
    }

    @Override
    public String toString() {
        return "NumberArray{" +
                "value=" + value +
                ", firstIndex=" + firstIndex +
                ", lastIndex=" + lastIndex +
                '}';
    }
}
